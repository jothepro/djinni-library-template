# Djinni Library Template Conan Package

Conan Package containing binaries built from [jothepro/djinni-library-template](https://github.com/jothepro/djinni-library-template).

Documentation of C++ interface: https://jothepro.github.io/djinni-library-template/cpp/

## Installation

1. Add the conan remote.
   ```bash
   conan remote add djinni_library_template https://gitlab.com/api/v4/projects/27897297/packages/conan
   ```
2. Add the library as dependency in your conanfile.
   ```ini
   [requires]
   my_djinni_library/0.0.1@jothepro/release
   ```